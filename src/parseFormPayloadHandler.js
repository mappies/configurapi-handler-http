let qs = require('querystring')

module.exports = function(event) 
{
        if(event.request.headers['content-type'] === 'application/x-www-form-urlencoded')
        {
                event.payload = Object.assign({}, qs.parse((event.payload || '').toString('utf-8')))
        }
};
