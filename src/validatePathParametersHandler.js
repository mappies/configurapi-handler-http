const ErrorResponse = require('configurapi').ErrorResponse;

module.exports = function validatePathParametersHandler(event)
{
    for (let i = 1; i < arguments.length; i++)
    {
        let validParameters = arguments[i.toString()];
        let parameterName = Object.keys(validParameters)[0];
        if (validParameters[parameterName].indexOf(event.params[parameterName]) === -1)
        {
            event.response = new ErrorResponse(`Path parameter '${parameterName}' must be one of ${validParameters[parameterName]}`, 404);
            return this.complete();
        }
    }
    return this.continue();
}