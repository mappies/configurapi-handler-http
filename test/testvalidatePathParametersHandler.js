const assert = require('chai').assert;
const sinon = require('sinon');
const Configurapi = require('configurapi');

const validatePathParametersHandler = require('../src/validatePathParametersHandler');

describe('validatePathParametersHandler', function() {
    it("valid path params", async function()
    {    
        let ev = sinon.mock(Configurapi.Event);
        ev.resolve = (e) => {return e;};
        ev.request = new Configurapi.Request();        
        ev.response = new Configurapi.Response();
        ev.params = {};
        ev.params['parameterone'] = 'p1v1';
        ev.params['parametertwo'] = 'p2v2';

        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        validatePathParametersHandler.apply(context, [ev,{ parameterone: [ 'p1v1','p1v2','p1v3' ]},{ parametertwo: [ 'p2v1','p2v2','p2v3' ] }]);

        assert.equal(200, ev.response.statusCode);
        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
    });

    it("Invalid path Params", async function()
    {    
        let ev = sinon.mock(Configurapi.Event);
        ev.resolve = (e) => {return e;};
        ev.request = new Configurapi.Request();        
        ev.response = new Configurapi.Response();
        ev.params = {};
        ev.params['parameterone'] = 'p1v1';
        ev.params['parametertwo'] = 'p2v4';

        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        validatePathParametersHandler.apply(context, [ev,{ parameterone: [ 'p1v1','p1v2','p1v3' ]},{ parametertwo: [ 'p2v1','p2v2','p2v3' ] }]);

        assert.equal(404, ev.response.statusCode);
        assert.isFalse(continueSpy.calledOnce);
        assert.isTrue(completeSpy.calledOnce);        
    });
});
